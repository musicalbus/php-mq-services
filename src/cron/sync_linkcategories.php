<?php
require_once __DIR__ . '/../../vendor/autoload.php';
require_once __DIR__ . '/../includes/include.php';
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

// set-up logging
$log = new Logger('worker_artistwiki');
$log->pushHandler(new StreamHandler(__DIR__ . '/../../logs/cron_linkcategories.log', $log_level));

// set-up musicalbus db conn
$conn = pg_connect("host=$database_host port=$database_port dbname=$database_name user=$database_user password=$database_password");
$brainz_conn = pg_connect("host=$musicbrainz_database_host port=$musicbrainz_database_port dbname=$musicbrainz_database_name user=$musicbrainz_database_user password=$musicbrainz_database_password");

$log->info("Starting sync link categories process");

$brainz_sql = "select id, gid, name, description
          from link_type
         where entity_type0 = 'artist'
           and entity_type1 = 'url'
         order by id";

$brainz_result = pg_query($brainz_conn, $brainz_sql);

if (!$brainz_result) {
    $log->error("Issue with no results from MusicBrainz data feed with link categories", array("error" => pg_last_error()));
    exit;
}

$bus_sql = "select id, mbid, name, description, musicbrainz_link_type_id
          from link_types
         where mbid is not NULL
         order by musicbrainz_link_type_id";

$bus_result = pg_query($conn, $bus_sql);

if (!$bus_result) {
    $log->error("Issue with no results from MusicalBus data feed with link categories", array("error" => pg_last_error()));
    exit;
}

$brainz_data = pg_fetch_all($brainz_result);
$bus_data = pg_fetch_all($bus_result);

$brainz_ct = count($brainz_data);
$bus_ct = count($bus_data);

for ($i = 0; $i < $brainz_ct; $i++) {
    $match = false;
    for ($j = 0; $j < $bus_ct; $j++) {
        if ($brainz_data[$i]['gid'] == $bus_data[$j]['mbid']) {
            $match = true;
            if (($brainz_data[$i]['name'] != $bus_data[$j]['name'])
                || ($brainz_data[$i]['id'] != $bus_data[$j]['musicbrainz_link_type_id'])
                || ($brainz_data[$i]['description'] != $bus_data[$j]['description'])) {
                $bus_sql_update = "update link_types set name = $2, description = $3, musicbrainz_link_type_id = $4 where mbid = $1";
                pg_prepare($conn, 'update_link_types', $bus_sql_update);
                if (pg_execute($conn, 'update_link_types', array($brainz_data[$i]['gid'], $brainz_data[$i]['name'], $brainz_data[$i]['description'], $brainz_data[$i]['id']))) {
                    $log->info("Updated link type into MusicalBus database", array("name" => $brainz_data[$i]['name'], "mbid" => $brainz_data[$i]['gid']));
                } else {
                    $log->error("Couldn't update link type into MusicalBus database", array("name" => $brainz_data[$i]['name'], "mbid" => $brainz_data[$i]['gid'], "error" => pg_last_error()));
                }
                pg_query($conn, "DEALLOCATE ALL");
            }
            break;
        }
    }
    if (!$match) {
        $bus_sql_insert = "insert into link_types (mbid, name, description, musicbrainz_link_type_id) values ($1, $2, $3, $4)";
        pg_prepare($conn, 'insert_link_types', $bus_sql_insert);
        if (pg_execute($conn, 'insert_link_types', array($brainz_data[$i]['gid'], $brainz_data[$i]['name'], $brainz_data[$i]['description'], $brainz_data[$i]['id']))) {
            $log->info("Inserted link type into MusicalBus database", array("name" => $brainz_data[$i]['name'], "mbid" => $brainz_data[$i]['gid']));
        } else {
            $log->error("Couldn't insert link type into MusicalBus database", array("name" => $brainz_data[$i]['name'], "mbid" => $brainz_data[$i]['gid'], "error" => pg_last_error()));
        }
        pg_query($conn, "DEALLOCATE ALL");
    }
}

$log->info("Finishing sync link categories process");

// clean-up
pg_close($conn);
pg_close($brainz_conn);
