<?php
require_once __DIR__ . '/../../vendor/autoload.php';
require_once __DIR__ . '/../includes/include.php';
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

// set-up logging
$log = new Logger('worker_relatedartists');
$log->pushHandler(new StreamHandler(__DIR__ . '/../../logs/worker_relatedartists.log', $log_level));

// set-up musicalbus db conn
$conn = pg_connect("host=$database_host port=$database_port dbname=$database_name user=$database_user password=$database_password");

// set-up rabbitmq conn
$mqconn = new AMQPStreamConnection($mq_host, $mq_port, $mq_user, $mq_password);
$channel = $mqconn->channel();
$channel->queue_declare('getRelatedArtists', false, true, false, false, false, ['x-max-priority' => ['I', 5]]);

$log->info('Starting Artist Links Add RabbitMQ Producer');

$ct = 0;

$sql = "select id, artist, mb_id
          from artists 
         where mb_id is not NULL
           and related_artist_no_sync = 0
           and sync_related_artists_date < extract(epoch from now()) - 604800";

$result = pg_query($conn, $sql);
if (!$result) {
    $log->error('SQL Error Related Artists Add RabbitMQ Producer');
    exit;
}

while ($row = pg_fetch_row($result)) {
    $msgtext = '{"artist_id":"'.$row[0].'", "artist":"'.htmlspecialchars($row[1]).'", "mb_id":"'.$row[2].'"}';

    if ($msgtext != null) {
        $msg = new AMQPMessage($msgtext, array('delivery_mode' => 2, 'priority' => '1'));
        $ct++;
        $channel->basic_publish($msg, '', 'getRelatedArtists');
    }
}

$log->info('Finished Related Artists Add RabbitMQ Producer', array("ct" => $ct));

pg_close($conn);
$channel->close();
$mqconn->close();
