<?php
require_once __DIR__ . '/../../vendor/autoload.php';
require_once __DIR__ . '/../includes/include.php';
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

// set-up logging
$log = new Logger('worker_artistwiki');
$log->pushHandler(new StreamHandler(__DIR__ . '/../../logs/worker_artistwiki.log', $log_level));

// set-up musicalbus db conn
$conn = pg_connect("host=$database_host port=$database_port dbname=$database_name user=$database_user password=$database_password");

// set-up rabbitmq conn
$mqconn = new AMQPStreamConnection($mq_host, $mq_port, $mq_user, $mq_password);
$channel = $mqconn->channel();
$channel->queue_declare('getWiki', false, true, false, false, false, ['x-max-priority' => ['I', 5]]);

$log->info('Starting Wikipedia Update RabbitMQ Producer');

$ct = 0;

$sql = "select artist_id,
    artist,
	substring(url from position('://' in url)+3 for 2) as locale,
	substring(url from position('wiki/' in url)+5) as article
	from artists, artists_links 
	where artist_id = artists.id
      and link_type_id = 2
      and url like '%wikipedia%'
	  and bio_no_sync = 0 
	  and sync_bio_date < (extract(epoch from now()) - 31536000)
	order by artist_id";

$result = pg_query($conn, $sql);
if (!$result) {
    $log->error('SQL Error Wikipedia Update RabbitMQ Producer');
    exit;
}

while ($row = pg_fetch_row($result)) {
    $msgtext = '{"artist_id":"'.$row[0].'", "artist":"'.htmlspecialchars($row[1]).'", "locale":"'.$row[2].'","article":"'.htmlspecialchars($row[3], ENT_QUOTES, 'UTF-8').'"}';

    if ($msgtext != null) {
        $msg = new AMQPMessage($msgtext, array('delivery_mode' => 2, 'priority' => '1'));
        $ct++;
        $channel->basic_publish($msg, '', 'getWiki');
    }
}

$log->info('Finished Wikipedia Update RabbitMQ Producer', array("ct" => $ct));


pg_close($conn);
$channel->close();
$mqconn->close();
