<?php
require_once __DIR__ . '/../../vendor/autoload.php';
require_once __DIR__ . '/../includes/include.php';
use PhpAmqpLib\Connection\AMQPStreamConnection;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

// set-up logging
$log = new Logger('worker_artistlinks');
$log->pushHandler(new StreamHandler(__DIR__ . '/../../logs/worker_artistlinks.log', $log_level));

// set-up musicalbus db conn
$bus_conn = pg_connect("host=$database_host port=$database_port dbname=$database_name user=$database_user password=$database_password");
$brainz_conn = pg_connect("host=$musicbrainz_database_host port=$musicbrainz_database_port dbname=$musicbrainz_database_name user=$musicbrainz_database_user password=$musicbrainz_database_password");

$sql = "select mbid, name, id from link_types where mbid is not NULL";
$result = pg_query($bus_conn, $sql);
$link_types = pg_fetch_all($result);
for ($i = 0; $i < count($link_types); $i++) {
    $link_categories[$link_types[$i]['mbid']] = $link_types[$i]['id'];
}

// set-up rabbitmq conn
$connection = new AMQPStreamConnection($mq_host, $mq_port, $mq_user, $mq_password);
$channel = $connection->channel();
$channel->queue_declare('getArtistLinks', false, true, false, false, false, ['x-max-priority' => ['I', 5]]);

function process($msg)
{
    global $log_level;
    global $log;
    global $bus_conn;
    global $brainz_conn;
    global $link_categories;

    // Example Received Data {"artist_id":"118"}
    $log->info('[x] Received ', array('body' => $msg->body));
    $artistlinks = json_decode($msg->body);
    $artist_id = $artistlinks->artist_id;
    $artist_name = $artistlinks->artist;
    $mb_id = $artistlinks->mb_id;

    if (strlen($mb_id) < 1) {
        $log->error("Error mb_id is invalid SQL", array("id" => $artist_id));
    }

    $sql = "SELECT url.gid as url_gid,
                   url.url,
                   link_type.gid AS link_type_gid
              FROM link,
                   link_type,
                   url,
                   artist,
                   l_artist_url
             WHERE artist.gid = $1 
                   AND l_artist_url.entity0 = artist.id
                   AND l_artist_url.entity1 = url.id
                   AND l_artist_url.link = link.id
                   AND link.link_type = link_type.id";

    if (!pg_prepare($brainz_conn, "brainz_artist_links", $sql)) {
        $log->error("Error processing brainz_artist_links SQL", array("artist_id" => $artist_id, "artist" => $artist_name, "mb_id" => $mb_id, "error" => pg_last_error()));
    }

    $result = pg_execute($brainz_conn, "brainz_artist_links", array($mb_id));
    $brainz_artist_links = pg_fetch_all($result);

    if (count($brainz_artist_links) < 1) {
        $log->warn("No brainz_artist_links data returned SQL", array("artist_id" => $artist_id, "artist" => $artist_name, "mb_id" => $mb_id));
    } else {
        $sql = "delete from artists_links where artist_id = $1 and mb_id is not NULL and length(mb_id) > 10";
        if (!pg_prepare($bus_conn, "delete_artist_links", $sql)) {
            $log->error("Error processing delete_artist_links SQL", array("artist_id" => $artist_id, "artist" => $artist_name, "error" => pg_last_error()));
        }
        $result = pg_execute($bus_conn, "delete_artist_links", array($artist_id));
        $log->info("Artist musicbrainz links have been deleted delete_artist_links SQL", array("artist_id" => $artist_id, "artist" => $artist_name, "ct" => pg_affected_rows($result)));
        
        for ($i = 0; $i < count($brainz_artist_links); $i++) {
            $sql = "insert into artists_links (mb_id, artist_id, link_type_id, url) values ($1, $2, $3, $4)";
            if (!pg_prepare($bus_conn, "insert_artist_link", $sql)) {
                $log->error("Error preparing insert_artist_link SQL", array("artist_id" => $artist_id, "artist" => $artist_name, "error" => pg_last_error()));
            }
            if (!pg_execute($bus_conn, "insert_artist_link", array($brainz_artist_links[$i]['url_gid'],
                                                                   $artist_id,
                                                                   $link_categories[$brainz_artist_links[$i]['link_type_gid']],
                                                                   $brainz_artist_links[$i]['url']))) {
                $log->error("Error executing insert_artist_link SQL", array("artist_id" => $artist_id, "artist" => $artist_name, "error" => pg_last_error()));
            }
            pg_query($bus_conn, "DEALLOCATE ALL");
        }
        $log->info("Artist musicbrainz links have been inserted SQL", array("artist_id" => $artist_id, "artist" => $artist_name, "ct" => count($brainz_artist_links)));
    }

    $sql = "update artists set sync_link_date=$1 where id=$2";
    $date = new DateTime();
    $update_timestamp = $date->getTimestamp();
    if (!pg_prepare($bus_conn, "update_date_links", $sql)) {
        $log->error("Error preparing update_data_links SQL", array("artist_id" => $artist_id, "artist" => $artist_name, "error" => pg_last_error()));
    }
    if (!pg_execute($bus_conn, "update_date_links", array($update_timestamp, $artist_id))) {
        $log->error("Error executing update_date_links SQL", array("artist_id" => $artist_id, "artist" => $artist_name, "error" => pg_last_error()));
    }

    pg_query($bus_conn, "DEALLOCATE ALL");
    pg_query($brainz_conn, "DEALLOCATE ALL");
    $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
};

/* Starting RabbitMQ Worker Process
 * pick-up one message at a time
 */
$channel->basic_qos(null, 1, null);
$channel->basic_consume('getArtistLinks', '', false, false, false, false, 'process');

while (count($channel->callbacks)) {
    $channel->wait();
}

// clean-up
pg_close($bus_conn);
pg_close($brainz_conn);
$channel->close();
$connection->close();
