<?php
require_once __DIR__ . '/../../vendor/autoload.php';
require_once __DIR__ . '/../includes/include.php';
use PhpAmqpLib\Connection\AMQPStreamConnection;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

// set-up logging
$log = new Logger('worker_artistspotify');
$log->pushHandler(new StreamHandler(__DIR__ . '/../../logs/worker_artistspotify.log', $log_level));

// set-up musicalbus db conn
$bus_conn = pg_connect("host=$database_host port=$database_port dbname=$database_name user=$database_user password=$database_password");
$brainz_conn = pg_connect("host=$musicbrainz_database_host port=$musicbrainz_database_port dbname=$musicbrainz_database_name user=$musicbrainz_database_user password=$musicbrainz_database_password");

// set-up rabbitmq conn
$connection = new AMQPStreamConnection($mq_host, $mq_port, $mq_user, $mq_password);
$channel = $connection->channel();
$channel->queue_declare('getArtistSpotify', false, true, false, false, false, ['x-max-priority' => ['I', 5]]);

$token = null;
$expires = null;
$artist_id = null;

function getToken()
{
    global $token;
    global $expires;
    $url = 'https://accounts.spotify.com/api/token';
    $method = 'POST';

    $credentials = "b8410539224740a18ba3c3ed5bb91fb4:69ccb45aa6144a7e95583589eedb9cd0";

    $opts = [
        "http" => [
            "method" => "POST",
            "header" => "Authorization: Basic " . base64_encode($credentials)."\r\n".
            "Content-type: application/x-www-form-urlencoded;charset=UTF-8\r\n",
            'content' => 'grant_type=client_credentials'
        ]
    ];

    $context = stream_context_create($opts);
    $response = file_get_contents($url, false, $context);

    $auth = json_decode($response, true);
    $token = $auth['access_token'];
    $expires = date("Y-m-d H:i:s", strtotime("+1 hours"));
}

function process($msg)
{
    global $log_level;
    global $log;
    global $bus_conn;
    global $brainz_conn;
    global $token;
    global $expires;

    $log->info('[x] Received ', array('body' => $msg->body));
    $artistspotify = json_decode($msg->body);
    $artist_id = $artistspotify->artist_id;
    $artist_name = $artistspotify->artist;
    $mb_id = $artistspotify->mb_id;

    $sql = "select release.name from release, artist_credit_name, artist where release.artist_credit = artist_credit_name.artist_credit and artist.id = artist_credit_name.artist and artist.gid = $1 group by release.name";

    if (!pg_prepare($brainz_conn, "brainz_artist_links", $sql)) {
        //     $log->error("Error processing brainz_artist_links SQL", array("id" => $artist_id, "mb_id" => $mb_id, "error" => pg_last_error()));
    }

    $result = pg_execute($brainz_conn, "brainz_artist_links", array($mb_id));
    $brainz_artist_links_results = pg_fetch_all($result);
    $brainz_artist_links = array();
    for ($i = 0; $i < count($brainz_artist_links_results); $i++) {
        array_push($brainz_artist_links, preg_replace("/[^a-zA-Z0-9]+/", "", strtolower($brainz_artist_links_results[$i]["name"])));
    }

    $now = date("Y-m-d H:i:s");
    if ($expires < $now) {
        getToken();
    }

    $url = "https://api.spotify.com/v1/search?q=".urlencode($artist_name)."&type=artist";

    $opts = [
        "http" => [
            "method" => "GET",
            "header" => "Authorization: Bearer $token"
        ]
    ];

    $context = stream_context_create($opts);
    $file = file_get_contents($url, false, $context);
    $data = json_decode($file);

    $max_ct = 0;
    $spotify_uri = null;
    $tested = 0;

    for ($i = 0; $i < count($data->artists->items); $i++) {
        $uri = $data->artists->items[$i]->id;
        $url = "https://api.spotify.com/v1/artists/$uri/albums";
        usleep(200000);
        $now = date("Y-m-d H:i:s");
        if ($expires < $now) {
            getToken();
        }
        $file = file_get_contents($url, false, $context);
        $albums = json_decode($file);
        $album_arr=array();
        for ($j = 0; $j < count($albums->items); $j++) {
            array_push($album_arr, preg_replace("/[^a-zA-Z0-9]+/", "", strtolower($albums->items[$j]->name)));
        }
        $unique_albums = array_unique($album_arr);
        $album_list = array_values($unique_albums);
        $ct = 0;
        for ($j = 0; $j < count($album_list); $j++) {
            for ($k = 0; $k < count($brainz_artist_links); $k++) {
                if ($album_list[$j] == $brainz_artist_links[$k]) {
                    $ct++;
                }
            }
        };
        $tested++;
        if ($ct > $max_ct) {
            $max_ct = $ct;
            $spotify_uri = $uri;
            if ($ct > 2) {
                break;
            }
        }
    }

    if (isset($spotify_uri)) {
        $sql = "delete from artists_links where artist_id = $1 and link_type_id = 31 and (mb_id is NULL or mb_id = '')";
        if (!pg_prepare($bus_conn, "delete_artist_link", $sql)) {
            $log->error("Error preparing delete_artist_link SQL", array("artist_id" => $artist_id, "artist" => $artist_name, "error" => pg_last_error()));
        }
        if (!pg_execute($bus_conn, "delete_artist_link", array($artist_id))) {
            $log->error("Error executing delete_artist_link SQL", array("artist_id" => $artist_id, "artist" => $artist_name, "error" => pg_last_error()));
        }
        pg_query($bus_conn, "DEALLOCATE ALL");
           
        $sql = "insert into artists_links (mb_id, artist_id, link_type_id, url) values (NULL, $1, 31, $2)";
        if (!pg_prepare($bus_conn, "insert_artist_link", $sql)) {
            $log->error("Error preparing insert_artist_link SQL", array("artist_id" => $artist_id, "artist" => $artist_name, "error" => pg_last_error()));
        }
        if (!pg_execute($bus_conn, "insert_artist_link", array($artist_id, "https://play.spotify.com/artist/" . $spotify_uri))) {
            $log->error("Error executing insert_artist_link SQL", array("artist_id" => $artist_id, "artist" => $artist_name, "error" => pg_last_error()));
        }
        pg_query($bus_conn, "DEALLOCATE ALL");

        $sql = "delete from artists_alt_ids where alt_ids_type_id = 1 and artist_id = $1";
        if (!pg_prepare($bus_conn, "delete_artist_alt_id", $sql)) {
            $log->error("Error preparing delete_artist_alt_id SQL", array("artist_id" => $artist_id, "artist" => $artist_name, "error" => pg_last_error()));
        }
        if (!pg_execute($bus_conn, "delete_artist_alt_id", array($artist_id))) {
            $log->error("Error executing delete_artist_alt_id SQL", array("artist_id" => $artist_id, "artist" => $artist_name, "error" => pg_last_error()));
        }
        pg_query($bus_conn, "DEALLOCATE ALL");
           
        $sql = "insert into artists_alt_ids (artist_id, alt_ids_type_id, alt_id) values ($1, 1, $2)";
        if (!pg_prepare($bus_conn, "insert_artist_alt_id", $sql)) {
            $log->error("Error preparing insert_artist_alt_id SQL", array("artist_id" => $artist_id, "artist" => $artist_name, "error" => pg_last_error()));
        }
        if (!pg_execute($bus_conn, "insert_artist_alt_id", array($artist_id, $spotify_uri))) {
            $log->error("Error executing insert_artist_alt_id SQL", array("artist_id" => $artist_id, "artist" => $artist_name, "error" => pg_last_error()));
        }

        $sql = "update artists set sync_spotify_uri_date=$1 where id=$2";
        $date = new DateTime();
        $update_timestamp = $date->getTimestamp();
        if (!pg_prepare($bus_conn, "update_date_spotify_uri", $sql)) {
            $log->error("Error preparing update_date_spotify_uri SQL", array("artist_id" => $artist_id, "artist" => $artist_name, "error" => pg_last_error()));
        }
        if (!pg_execute($bus_conn, "update_date_spotify_uri", array($update_timestamp, $artist_id))) {
            $log->error("Error executing update_date_spotify_uri SQL", array("artist_id" => $artist_id, "artist" => $artist_name, "error" => pg_last_error()));
        }
        pg_query($bus_conn, "DEALLOCATE ALL");
    }

    pg_query($bus_conn, "DEALLOCATE ALL");
    pg_query($brainz_conn, "DEALLOCATE ALL");
    $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
}

/* Starting RabbitMQ Worker Process
 * pick-up one message at a time
 */
$channel->basic_qos(null, 1, null);
$channel->basic_consume('getArtistSpotify', '', false, false, false, false, 'process');

while (count($channel->callbacks)) {
    $channel->wait();
}

// clean-up
pg_close($bus_conn);
pg_close($brainz_conn);
$channel->close();
$connection->close();
