<?php
require_once __DIR__ . '/../../vendor/autoload.php';
require_once __DIR__ . '/../includes/include.php';
use PhpAmqpLib\Connection\AMQPStreamConnection;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

// set-up logging
$log = new Logger('worker_artistwiki');
$log->pushHandler(new StreamHandler(__DIR__ . '/../../logs/worker_photoresizer.log', $log_level));

// set-up rabbitmq conn
$mqconn = new AMQPStreamConnection($mq_host, $mq_port, $mq_user, $mq_password);
$channel = $mqconn->channel();
$channel->queue_declare('resizePhoto', false, true, false, false, false);

function resize($newWidth, $targetFile, $originalFile)
{
    global $log;
    $info = getimagesize($originalFile);
    $mime = $info['mime'];

    switch ($mime) {
            case 'image/jpeg':
                    $image_create_func = 'imagecreatefromjpeg';
                    $image_save_func = 'imagejpeg';
                    $new_image_ext = 'jpg';
                    break;

            case 'image/png':
                    $image_create_func = 'imagecreatefrompng';
                    $image_save_func = 'imagepng';
                    $new_image_ext = 'png';
                    break;

            case 'image/gif':
                    $image_create_func = 'imagecreatefromgif';
                    $image_save_func = 'imagegif';
                    $new_image_ext = 'gif';
                    break;

            default:
                    $log->error("Error processing photo unknown photo format.", array("original_photo" => $originalFile));
    }

    if (!empty($new_image_ext)) {
        $img = $image_create_func($originalFile);
        list($width, $height) = getimagesize($originalFile);

        $newHeight = ($height / $width) * $newWidth;
        $tmp = imagecreatetruecolor($newWidth, $newHeight);
        imagecopyresampled($tmp, $img, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);

        if (file_exists($targetFile)) {
            unlink($targetFile);
        }
        $image_save_func($tmp, "$targetFile");
    }
}

function process($msg)
{
    global $log;
    $log->info('[x] Received ', array('body' => $msg->body));
    $imageinfo = json_decode($msg->body);
    resize($imageinfo->width, $imageinfo->destination, $imageinfo->original_photo);
    $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
}

/* Starting RabbitMQ Worker Process
 * pick-up one message at a time
 */
$channel->basic_qos(null, 1, null);
$channel->basic_consume('resizePhoto', '', false, false, false, false, 'process');

while (count($channel->callbacks)) {
    $channel->wait();
}

// clean-up
$channel->close();
$mqconn->close();
