<?php
require_once __DIR__ . '/../../vendor/autoload.php';
require_once __DIR__ . '/../includes/include.php';
use PhpAmqpLib\Connection\AMQPStreamConnection;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

// set-up logging
$log = new Logger('worker_artistwiki');
$log->pushHandler(new StreamHandler(__DIR__ . '/../../logs/worker_artistwiki.log', $log_level));

// set-up musicalbus db conn
$conn = pg_connect("host=$database_host port=$database_port dbname=$database_name user=$database_user password=$database_password");

// set-up rabbitmq conn
$connection = new AMQPStreamConnection($mq_host, $mq_port, $mq_user, $mq_password);
$channel = $connection->channel();
$channel->queue_declare('getWiki', false, true, false, false, false, ['x-max-priority' => ['I', 5]]);

function getArticle($article, $locale)
{
    $wikipedia = new \Casinelli\Wikipedia\Wikipedia;
    $wikipedia->setLocale($locale);
    return $wikipedia->search($article)->getArticle();
}

function process($msg)
{
    global $log_level;
    global $log;
    global $conn;

    // Example Received Data {"artist_id":"118","locale":"en","article":"Radiohead"}
    $log->info('[x] Received ', array('body' => $msg->body));
    $artistwiki = json_decode($msg->body);

    $sql = "select bio_no_sync from artists where id = $1";

    if (!pg_prepare($conn, "bio_sync", $sql)) {
        $log->error("Error processing bio_no_sync SQL", array("artist_id" => $artistwiki->artist_id, "artist" => $artistwiki->artist, "error" => pg_last_error()));
    }

    $result = pg_execute("bio_sync", array($artistwiki->artist_id));
    $bio_sync_data = pg_fetch_array($result);

    if ($bio_sync_data[0] == "0" || $bio_sync_data[0] == "") {
        usleep(500); // slow down script to take it easy on wikipedia
        $wikidata = getArticle($artistwiki->article, $artistwiki->locale);
        $log->debug('[x] Wikipedia API Received ', array('data' => $wikidata));

        if (!empty($wikidata)) {
            $sql = "update artists set bio=$1, sync_bio_date=$2 where id=$3";
            $date = new DateTime();
            $update_timestamp = $date->getTimestamp();
            if (!pg_prepare($conn, "update_bio", $sql)) {
                $log->error("Error preparing update_bio SQL", array("artist_id" => $artistwiki->artist_id, "artist" => $artistwiki->artist, "error" => pg_last_error()));
            }
            if (!pg_execute("update_bio", array($wikidata, $update_timestamp, $artistwiki->artist_id))) {
                $log->error("Error executing update_bio SQL", array("artist_id" => $artistwiki->artist_id, "artist" => $artistwiki->artist, "error" => pg_last_error()));
            }
        } else {
            $log->error("Error No Wikipedia Data Found", array("artist_id" => $artistwiki->artist_id, "artist" => $artistwiki->artist, "article" => $artistwiki->article, "error" => pg_last_error()));
        }
    }
    pg_query($conn, "DEALLOCATE ALL");
    $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
    //sleep(substr_count($msg->body, '.'));
};

/* Starting RabbitMQ Worker Process
 * pick-up one message at a time
 */
$channel->basic_qos(null, 1, null);
$channel->basic_consume('getWiki', '', false, false, false, false, 'process');

while (count($channel->callbacks)) {
    $channel->wait();
}

// clean-up
pg_close($conn);
$channel->close();
$connection->close();
