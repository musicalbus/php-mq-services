<?php
require_once __DIR__ . '/../../vendor/autoload.php';
require_once __DIR__ . '/../includes/include.php';
use PhpAmqpLib\Connection\AMQPStreamConnection;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

// set-up logging
$log = new Logger('worker_artisttags');
$log->pushHandler(new StreamHandler(__DIR__ . '/../../logs/worker_artisttags.log', $log_level));

// set-up musicalbus db conn
$bus_conn = pg_connect("host=$database_host port=$database_port dbname=$database_name user=$database_user password=$database_password");
$brainz_conn = pg_connect("host=$musicbrainz_database_host port=$musicbrainz_database_port dbname=$musicbrainz_database_name user=$musicbrainz_database_user password=$musicbrainz_database_password");

// set-up rabbitmq conn
$connection = new AMQPStreamConnection($mq_host, $mq_port, $mq_user, $mq_password);
$channel = $connection->channel();
$channel->queue_declare('getArtistTags', false, true, false, false, false, ['x-max-priority' => ['I', 5]]);

function process($msg)
{
    global $log_level;
    global $log;
    global $bus_conn;
    global $brainz_conn;

    // Example Received Data {"artist_id":"118"}
    $log->info('[x] Received ', array('body' => $msg->body));
    $artisttags = json_decode($msg->body);
    $artist_id = $artisttags->artist_id;
    $artist_name = $artisttags->artist;
    $mb_id = $artisttags->mb_id;

    if (strlen($mb_id) < 1) {
        $log->error("Error mb_id is invalid SQL", array("id" => $artist_id));
    }

    $sql = "select tag.name, row_number() over(order by count desc) as rank
              from artist, artist_tag, tag
             where artist.id = artist_tag.artist
               and artist_tag.tag = tag.id
               and artist.gid = $1
             order by artist_tag.count desc";

    if (!pg_prepare($brainz_conn, "brainz_artist_tags", $sql)) {
        $log->error("Error processing brainz_artist_tags SQL", array("id" => $artist_id, "artist" => $artist_name, "mb_id" => $mb_id, "error" => pg_last_error()));
    }

    $result = pg_execute($brainz_conn, "brainz_artist_tags", array($mb_id));
    $brainz_artist_tags = pg_fetch_all($result);
    $ct = pg_num_rows($result);
    
    $log->info("Artist musicbrainz tags returned:", array("id" => $artist_id, "artist" => $artist_name, "ct" => $ct));

    if ($ct < 1) {
        $log->warn("No brainz_artist_tags data returned SQL", array("id" => $artist_id, "artist" => $artist_name, "mb_id" => $mb_id));
    } else {
        $sql = "delete from artists_tags where artist_id = $1 and artist_id > 1484";
        if (!pg_prepare($bus_conn, "delete_artist_tags", $sql)) {
            $log->error("Error processing delete_artist_tags SQL", array("id" => $artist_id, "artist" => $artist_name, "error" => pg_last_error()));
        }
        $result = pg_execute($bus_conn, "delete_artist_tags", array($artist_id));
        $log->info("Artist musicbrainz tags have been deleted delete_artist_tags SQL", array("id" => $artist_id, "artist" => $artist_name, "ct" => pg_affected_rows($result)));
        
        for ($i = 0; $i < count($brainz_artist_tags); $i++) {
            if ($brainz_artist_tags[$i]['name'] != "" && !empty($brainz_artist_tags[$i]['name'])) {
                $sql = "insert into artists_tags (artist_id, tag_id, rank) values ($1, get_or_insert_tag_id($2), $3)";
                if (!pg_prepare($bus_conn, "insert_artist_tag", $sql)) {
                    $log->error("Error preparing insert_artist_tag SQL", array("id" => $artist_id, "artist" => $artist_name, "error" => pg_last_error()));
                }
                if (!pg_execute($bus_conn, "insert_artist_tag", array( $artist_id,
                                                                       $brainz_artist_tags[$i]['name'],
                                                                       $brainz_artist_tags[$i]['rank']))) {
                    $log->error("Error executing insert_artist_tag SQL", array("id" => $artist_id, "artist" => $artist_name, "error" => pg_last_error()));
                }
            }
            pg_query($bus_conn, "DEALLOCATE ALL");
        }
        $log->info("Artist musicbrainz tags have been inserted SQL", array("id" => $artist_id, "artist" => $artist_name, "ct" => $ct));
    }

    $sql = "update artists set sync_tags_date=$1 where id=$2";
    $date = new DateTime();
    $update_timestamp = $date->getTimestamp();
    if (!pg_prepare($bus_conn, "update_date_tags", $sql)) {
        $log->error("Error preparing update_data_tags SQL", array("id" => $artist_id, "artist" => $artist_name, "error" => pg_last_error()));
    }
    if (!pg_execute($bus_conn, "update_date_tags", array($update_timestamp, $artist_id))) {
        $log->error("Error executing update_date_tags SQL", array("id" => $artist_id, "artist" => $artist_name, "error" => pg_last_error()));
    }

    pg_query($bus_conn, "DEALLOCATE ALL");
    pg_query($brainz_conn, "DEALLOCATE ALL");
    $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
};

/* Starting RabbitMQ Worker Process
 * pick-up one message at a time
 */
$channel->basic_qos(null, 1, null);
$channel->basic_consume('getArtistTags', '', false, false, false, false, 'process');

while (count($channel->callbacks)) {
    $channel->wait();
}

// clean-up
pg_close($bus_conn);
pg_close($brainz_conn);
$channel->close();
$connection->close();
