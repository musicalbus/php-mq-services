<?php
require_once __DIR__ . '/../../vendor/autoload.php';
require_once __DIR__ . '/../includes/include.php';
use PhpAmqpLib\Connection\AMQPStreamConnection;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

// set-up logging
$log = new Logger('worker_relatedartists');
$log->pushHandler(new StreamHandler(__DIR__ . '/../../logs/worker_relatedartists.log', $log_level));

// set-up musicalbus db conn
$conn = pg_connect("host=$database_host port=$database_port dbname=$database_name user=$database_user password=$database_password");

// set-up rabbitmq conn
$connection = new AMQPStreamConnection($mq_host, $mq_port, $mq_user, $mq_password);
$channel = $connection->channel();
$channel->queue_declare('getRelatedArtists', false, true, false, false, false, ['x-max-priority' => ['I', 5]]);

function process($msg)
{
    global $log_level;
    global $log;
    global $conn;
    global $lastfm_key;
    // Example Received Data {"artist_id":"118","mb_id":"12252-2262-2126262e"}
    $log->info('[x] Received ', array('body' => $msg->body));
    $relatedartists = json_decode($msg->body);
    $artist_id = $relatedartists->artist_id;
    $artist_name = $relatedartists->artist;
    $mb_id = $relatedartists->mb_id;

    $sql = "select related_artist_no_sync from artists where id = $1";

    if (!pg_prepare($conn, "sync", $sql)) {
        $log->error("Error processing related_artist_no_sync SQL", array("artist_id" => $artist_id, "artist" => $artist_name, "error" => pg_last_error()));
    }

    $result = pg_execute("sync", array($artist_id));
    $sync_data = pg_fetch_array($result);

    if ($sync_data[0] == "0" || $sync_data[0] == "") {
        try {
            $url = "http://ws.audioscrobbler.com/2.0/?method=artist.getsimilar&mbid=$mb_id&api_key=$lastfm_key&format=json";
            $json = file_get_contents($url);
            usleep(1000); // slow down script to take it easy on lastfm
            $related_artists_data_obj = json_decode($json);
            $related_artists_data = $related_artists_data_obj->similarartists->artist;
            $related_artists_data_ct = count($related_artists_data);
            $sql = "select related_artist_mb_id from related_artists where artist_id = $1";

            if (!pg_prepare($conn, "mb_data", $sql)) {
                $log->error("Error processing mb_data SQL", array("artist_id" => $artist_id, "artist" => $artist_name, "error" => pg_last_error()));
            }

            $result = pg_execute("mb_data", array($artist_id));
            $mb_data = pg_fetch_all($result);
            $mb_ct = count($mb_data);
            $new_ct = 0;
            for ($i = 0; $i < $related_artists_data_ct; $i++) {
                $found = false;
                if (isset($related_artists_data[$i]->mbid)) {
                    for ($j = 0; $j < $mb_ct; $j++) {
                        if ($related_artists_data[$i]->mbid == $mb_data[$j]['related_artist_mb_id']) {
                            $found = true;
                            break;
                        }
                    }
                    if (!$found) {
                        $sql = "select id from artists where mb_id = $1 and not exists (select 'x' from related_artists where artists.id = related_artist_id and artist_id = $2)";

                        if (!pg_prepare($conn, "get_artist_id", $sql)) {
                            $log->error("Error processing get_artist_id for related artist SQL", array("related_artist_mb_id" => $related_artists_data[$i]->mbid, "error" => pg_last_error()));
                        }

                        $result = pg_execute("get_artist_id", array($related_artists_data[$i]->mbid, $artist_id));
                        $data = pg_fetch_array($result);
                        if (!empty($data['id'])) {
                            $sql = "insert into related_artists (artist_id, related_artist_id, related_artist_mb_id, match_success) values ($1, $2, $3, $4)";

                            if (!pg_prepare($conn, "insert_related_artist", $sql)) {
                                $log->error("Error processing insert_related_artist for related artist SQL", array("artist_id" => $artist_id, "artist" => $artist_name, "related_artist_mb_id" => $related_artists_data[$i]->mbid, "error" => pg_last_error()));
                            }

                            if (!pg_execute("insert_related_artist", array($artist_id, $data['id'], $related_artists_data[$i]->mbid, $related_artists_data[$i]->match))) {
                                $log->error("Error executing insert_related_artist for related artist SQL", array("artist_id" => $artist_id, "artist" => $artist_name, "related_artist_mb_id" => $related_artists_data[$i]->mbid, "error" => pg_last_error()));
                            }
                        }
                        pg_query($conn, "DEALLOCATE ALL");
                        $new_ct++;
                    }
                }
            }
        } catch (Exception $e) {
            $log->error('Error grabbing lastfm data:', array('artist_id' => $artist_id, "artist" => $artist_name, 'url' => $url, 'Exception' => $e->getMessage()));
        }

        if ($new_ct > 0) {
            $sql = "update artists set sync_related_artists_date=$1 where id=$2";
            $date = new DateTime();
            $update_timestamp = $date->getTimestamp();
            if (!pg_prepare($conn, "update_date_related", $sql)) {
                $log->error("Error preparing update_data_related SQL", array("artist_id" => $artist_id, "artist" => $artist_name, "error" => pg_last_error()));
            }
            if (!pg_execute($conn, "update_date_related", array($update_timestamp, $artist_id))) {
                $log->error("Error executing update_date_related SQL", array("artist_id" => $artist_id, "artist" => $artist_name, "error" => pg_last_error()));
            }
        }
    }
    pg_query($conn, "DEALLOCATE ALL");
    $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
};

/* Starting RabbitMQ Worker Process
 * pick-up one message at a time
 */
$channel->basic_qos(null, 1, null);
$channel->basic_consume('getRelatedArtists', '', false, false, false, false, 'process');

while (count($channel->callbacks)) {
    $channel->wait();
}

// clean-up
pg_close($conn);
$channel->close();
$connection->close();
